package com.company;

import java.util.UUID;

public class CheckResult {
    public int score;
    public UUID taskId;
    /**
     * Имя сервиса, который выполнил проверку.
     */
    public String checkService;

    @Override
    public String toString() {
        return String.format("%s: [%s] by %s", taskId, score, checkService);
    }
}