package com.company;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * Ученики выполняют контрольные работы (экзамены, контрольные работы,
 * обычные задания) и сдают их на проверку.
 * Проверка заданий автоматизирована, и выполняется отдельным сервисом, который
 * в результате проверки выдает количество заработанных баллов (от 1 до 10).
 * <p>
 * Задания на проверку должны быть обработаны в порядке приоритетов:
 * <ul>
 * <li>Сначала экзамены, затем контрольные работы и далее обычные задания</li>
 * <li>Сначала задания учеников A классов, затем Б классов и так далее</li>
 * <li>Далее по алфавиту</li>
 * </ul>
 * <p>
 * Необходимо реализовать проверку заданий от учеников.
 */

public interface TaskService {
    void addTask(Task task);

    CheckResult getCheckResult(UUID taskId);

    Collection<Task> tasks(int limit);

    void setTaskChecked(String checkService, Map<UUID, Integer> checkResult);
}