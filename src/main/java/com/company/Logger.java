package com.company;

import java.io.PrintStream;

public class Logger {

    private static final PrintStream stream = System.out;

    public static void log(Object object) {
        stream.println(object == null ? "null" : object.toString());
    }
}