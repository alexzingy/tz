package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TaskAdder extends Thread {
    private final TaskService taskService;

    public TaskAdder(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            List<Task> taskList = new ArrayList<>();
            taskList.add(TaskFactory.create("373fa9e0-d0c1-43c0-8332-b54982df4a8d", 'A', "Ivanov", TaskType.TASK));
            taskList.add(TaskFactory.create("a7050e4e-b9f8-400a-ad36-223d435649a2", 'A', "Zucchin", TaskType.EXAM));
            taskList.add(TaskFactory.create("0ab0cda2-38a3-4b25-b1df-8d2ae7b74eeb", 'B', "Ivanov", TaskType.TEST));
            taskList.add(TaskFactory.create("736e74f1-b2ac-4f75-9dae-d77110ec5b69", 'A', "Ivanov", TaskType.TEST));
            taskList.add(TaskFactory.create("0771678f-90f3-4f06-9bc4-a7c904486072", 'A', "Sidorov", TaskType.TEST));

            Collections.shuffle(taskList);

            taskList.forEach(taskService::addTask);

            Util.sleep(1_000);

            CheckResult checkResult = taskService.getCheckResult(UUID.fromString("373fa9e0-d0c1-43c0-8332-b54982df4a8d"));
            Logger.log(checkResult);
        }
    }
}