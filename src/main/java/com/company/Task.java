package com.company;


import java.util.UUID;

public class Task implements Comparable<Task> {
    public UUID taskId;
    /**
     * 1 - экзамен
     * 2 - контрольная
     * 3 - обычное задание
     */
    public int taskTypeId;
    public char eduClassLetter;
    public String studentFio;

    @Override
    public int compareTo(Task o) {
        if (this.taskTypeId > o.taskTypeId) {
            return 1;
        }

        if (this.taskTypeId < o.taskTypeId) {
            return -1;
        }

        if (this.eduClassLetter < o.eduClassLetter) {
            return -1;
        }

        if (this.eduClassLetter > o.eduClassLetter) {
            return 1;
        }

        return this.studentFio.compareTo(o.studentFio);
    }

    @Override
    public String toString() {
        return String.format("%s: %s-%s", TaskType.of(taskTypeId).name(), eduClassLetter, studentFio);
    }
}