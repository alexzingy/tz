package com.company;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TaskChecker extends Thread {
    private final TaskService taskService;
    private final String name;

    public static final int INTERVAL = 500;
    public static final int MAX_POLL_RECORDS = 5;

    public TaskChecker(TaskService taskService, String name) {
        this.taskService = taskService;
        this.name = name;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Logger.log("Start checking tasks");

            Collection<Task> tasks = taskService.tasks(MAX_POLL_RECORDS);

            Map<UUID, Integer> checkMap = new HashMap<>();
            for (Task task : tasks) {
                Logger.log(String.format("Processing task %s", task));

                checkMap.put(task.taskId, getScore());
            }

            taskService.setTaskChecked(name, checkMap);

            Util.sleep(INTERVAL);
        }
    }

    private int getScore() {
        return (int) (Math.random() * 100 % 10) + 1;
    }
}