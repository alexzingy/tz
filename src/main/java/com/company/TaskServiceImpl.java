package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Сервис хранящий и предоставляющий информацию о задачах
 * Также выполняет сортировку задач перед тем как их предоставить в соответствии с контрактом интерфейса
 */
public class TaskServiceImpl implements TaskService {
    private final Queue<Task> taskQueue = new PriorityBlockingQueue<>();
    private final Map<UUID, CheckResult> checkResultRegistry = new ConcurrentHashMap<>();

    @Override
    public void addTask(Task task) {
        taskQueue.add(task);
    }

    @Override
    public Collection<Task> tasks(int limit) {
        int index = 0;
        List<Task> taskList = new ArrayList<>();

        Task currentTask;

        while (index < limit && (currentTask = taskQueue.poll()) != null) {
            taskList.add(currentTask);
            index++;
        }

        return taskList;
    }

    @Override
    public void setTaskChecked(String checkService, Map<UUID, Integer> checkResultMap) {
        for (Map.Entry<UUID, Integer> uuidIntegerEntry : checkResultMap.entrySet()) {
            CheckResult checkResult = new CheckResult();
            checkResult.score = uuidIntegerEntry.getValue();
            checkResult.taskId = uuidIntegerEntry.getKey();
            checkResult.checkService = checkService;

            checkResultRegistry.put(uuidIntegerEntry.getKey(), checkResult);
        }
    }

    @Override
    public CheckResult getCheckResult(UUID taskId) {
        return checkResultRegistry.get(taskId);
    }
}