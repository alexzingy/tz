package com.company;

public class $ {
    public static TaskService taskService;
    public static TaskChecker taskChecker;
    public static TaskAdder taskAdder;

    public static void init() {
        taskService = new TaskServiceImpl();
        taskChecker = new TaskChecker($.taskService, "checker-A");
        taskAdder = new TaskAdder($.taskService);
    }
}