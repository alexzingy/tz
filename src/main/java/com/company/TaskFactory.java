package com.company;

import java.util.UUID;

/**
 * Инкапсулирует создание тасок
 */
public class TaskFactory {

    public static Task create(String uuid, char letter, String fio, TaskType taskType) {
        return create(UUID.fromString(uuid), letter, fio, taskType);
    }

    public static Task create(UUID uuid, char letter, String fio, TaskType taskType) {
        Task task = new Task();
        task.eduClassLetter = letter;
        task.studentFio = fio;
        task.taskId = uuid;
        task.taskTypeId = taskType.getCode();

        return task;
    }
}