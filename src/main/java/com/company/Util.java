package com.company;

/**
 * Утиль
 */
public class Util {
    public static void sleep(int interval) {
        try {
            Thread.sleep(interval);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}