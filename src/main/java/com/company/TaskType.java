package com.company;

import java.util.Arrays;

/**
 * Вспомогательный enum для оборачивания стандартного API
 *
 * 1 - экзамен
 * 2 - контрольная
 * 3 - обычное задание
 */
public enum TaskType {
    EXAM(1),
    TEST(2),
    TASK(3);

    private final int priority;

    TaskType(int priority) {
        this.priority = priority;
    }

    public int getCode() {
        return priority;
    }

    public static TaskType of(int type) {
        return Arrays
                .stream(TaskType.values())
                .filter(taskType -> taskType.priority == type)
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        String.format("Cannot find task object by code %s", type)));
    }
}